//
// Created by lida on 23.09.18.
//
#include <memory>
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <deque>
#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <sstream>
#include <unordered_map>
#include <vector>

struct AhoCorasickNode {
  // Stores ids of strings which are ended at this node.
  std::vector<size_t> terminalIds;
  // Stores tree structure of nodes.
  std::map<char, AhoCorasickNode> trieTransitions;//Children
  // Stores cached transitions of the automaton, contains
  // only pointers to the elements of trieTransitions.
  std::unordered_map<char, AhoCorasickNode *> automatonTransitionsCache;
  AhoCorasickNode *suffixLink = nullptr;
  AhoCorasickNode *terminalSuffixLink = nullptr;

};

//Попробовать поискать в боре, если его там нет,то поискать рекурсивно по суффиксной ссылке сохранить в кеше и вернуть
AhoCorasickNode *GetAutomatonTransition(AhoCorasickNode *node, char ch) {

    //auto& nextNode = node->automatonTransitionsCache.emplace(ch, )

    if (node->automatonTransitionsCache.count(ch) != 1) { //Ищем есть ли этот переход в боре
        AhoCorasickNode *transitionNode;
        auto it = node->trieTransitions.find(ch);

        if (it != node->trieTransitions.end()) {
            transitionNode = &it->second;
        } else if (node->suffixLink == node) {
            transitionNode = node;
        } else {
            //Ищем рекурсивно по суффикс ссылке
            transitionNode = GetAutomatonTransition(node->suffixLink, ch);
        }
        node->automatonTransitionsCache.insert(std::pair<char, AhoCorasickNode *>(ch, transitionNode));
    }
    return node->automatonTransitionsCache.find(ch)->second; //Возвращаем астоматный переход
};

class NodeReference {
public:
    NodeReference() = default;
    explicit NodeReference(AhoCorasickNode *node)
        : node_(node) {}

    NodeReference Next(char ch) const {
        return NodeReference(GetAutomatonTransition(node_, ch));
    };

    template<class Callback>
    void ForEachMatch(Callback cb) const {
        for (AhoCorasickNode *suffNode = node_; suffNode->suffixLink != suffNode;
             suffNode = suffNode->terminalSuffixLink) {
            if (!suffNode->terminalIds.empty()) {
                for (auto terminalId : suffNode->terminalIds) {
                    cb(terminalId);
                }
            }
        }
    };

private:
    AhoCorasickNode *node_ = nullptr;
};

class AhoCorasick {
public:
    AhoCorasick() = default;
    AhoCorasick(const AhoCorasick &) = delete;
    AhoCorasick &operator=(const AhoCorasick &) = delete;
    AhoCorasick(AhoCorasick &&) = delete;
    AhoCorasick &operator=(AhoCorasick &&) = delete;

    NodeReference getRoot() {
        NodeReference nodeReference(&root_);
        return nodeReference;
    };

private:
    friend class AhoCorasickBuilder;
    AhoCorasickNode root_;
};

class AhoCorasickBuilder {
public:
    AhoCorasickBuilder() = default;

    void AddString(std::string string, size_t id) {
        strings.push_back(std::move(string));
        ids.push_back(id);
    }

    //Самая важная функция, ее вызываем
    std::unique_ptr<AhoCorasick> Build() {
        auto automaton = std::make_unique<AhoCorasick>();
        for (size_t i = 0; i < strings.size(); ++i) {
            AddString(&automaton->root_, strings[i], ids[i]);
        }
        CalculateLinks(&automaton->root_);
        return automaton;
    }

private:
    static void AddString(
        AhoCorasickNode *root,
        const std::string &string,
        size_t id
    );

    static AhoCorasickNode *getTerminalLink(AhoCorasickNode *node);
    static AhoCorasickNode *getSuffixLink(AhoCorasickNode *node, AhoCorasickNode *parentNode, char parentChar);
    static void CalculateLinks(AhoCorasickNode *root);
    std::vector<std::string> strings;
    std::vector<size_t> ids;

};

void AhoCorasickBuilder::AddString(AhoCorasickNode *root, const std::string &string, size_t id) {
    AhoCorasickNode *currentNode = root;
    for (char ch : string) {
        currentNode = &currentNode->trieTransitions[ch];
    }
    currentNode->terminalIds.push_back(id);
}

//С ее помощью мы ищем суффиксные ссылки и терминальные суффиксные ссылки
void AhoCorasickBuilder::CalculateLinks(AhoCorasickNode *root) {
    //Обходим при помощи BFS
    root->suffixLink = root;//Суффиксная ссылка на корень - сам корень
    root->terminalSuffixLink = root;//Терминальная ссылка на корень - сам корень
    std::queue<AhoCorasickNode *> queue;//Создали очередь
    queue.push(root);//Положили первый элемент - корень

    while (!queue.empty()) {
        AhoCorasickNode *currentNode = queue.front();//Взяли первую из вершин
        queue.pop();//Тут же убрали ее из очереди
        //Пробегаемся по всем ее соседям(сыновьям)
        for (auto &element: currentNode->trieTransitions) {

            char parentChar = element.first;//Буква на ребре
            AhoCorasickNode &node = element.second;

            //Для суффиксных ссылок. Вынеси потом в отдельную функцию
            node.suffixLink = getSuffixLink(&node, currentNode, parentChar);

            //Построение терминальных ссылок
            node.terminalSuffixLink = getTerminalLink(&node);

            queue.push(&node);
        }
    }
}

AhoCorasickNode *AhoCorasickBuilder::getTerminalLink(AhoCorasickNode *node) {
    if (node->terminalSuffixLink == nullptr) {
        if (!node->suffixLink->terminalIds.empty()) {
            node->terminalSuffixLink = node->suffixLink;
        } else {
            node->terminalSuffixLink = getTerminalLink(node->suffixLink);
        }
    }
    return node->terminalSuffixLink;
}
AhoCorasickNode *AhoCorasickBuilder::getSuffixLink(AhoCorasickNode *node,
                                                   AhoCorasickNode *parentNode,
                                                   char parentChar) {
    if (node->suffixLink == nullptr) {
        if (parentNode->suffixLink == parentNode) {
            node->suffixLink = parentNode;
        } else {
            node->suffixLink = GetAutomatonTransition(parentNode->suffixLink, parentChar);
        }
    }
    return node->suffixLink;
}

//Для работы с исходными строками
std::vector<std::string> Split(const std::string &pattern, char delimiter, AhoCorasickBuilder &ahoCorasickBuilder) {
    //Парсит строку и возвращает вектор с подшаблонами
    std::vector<std::string> patterns;
    std::string s;
    for (size_t i = 0; i < pattern.length(); ++i) {
        if (pattern[i] == delimiter) {
            if (!s.empty()) {
                patterns.push_back(s);
                ahoCorasickBuilder.AddString(s, i - 1);
                s.clear();
            }
        } else {
            s += pattern[i];
        }
    }
    if (!s.empty()) {
        patterns.push_back(s);
        ahoCorasickBuilder.AddString(s, pattern.length() - 1);
    }
    return patterns;
};

class WildcardMatcher {
public:
    WildcardMatcher(const std::string &pattern, char wildcard);

    // Resets the matcher. Call allows to abandon all data which was already
    // scanned, a new stream can be scanned afterwards.
    void Reset() {
        occurrencesByOffset_.clear();
        state_ = automaton_->getRoot();

    }
    template<class Callback>
    void Scan(char character, Callback onMatch);

private:
    void UpdateWordOccurrencesCounters(size_t patternId) { //PatternId - конец вхождения шаблона
        size_t s = patternId + 1;
        if (occurrencesByOffset_.size() >= s) {
            occurrencesByOffset_[occurrencesByOffset_.size() - s] += 1;
        }

    }

    void ShiftWordOccurrencesCounters() {
        occurrencesByOffset_.push_back(0);
        if (occurrencesByOffset_.size() > patternLength_) {
            occurrencesByOffset_.pop_front();
        }
    }

    // Storing only O(|pattern|) elements allows us
    // to consume only O(|pattern|) memory for the matcher.
    std::deque<size_t> occurrencesByOffset_;
    size_t numberOfWords_;
    size_t patternLength_;
    std::unique_ptr<AhoCorasick> automaton_;
    NodeReference state_;
};

template<class Callback>
void WildcardMatcher::Scan(char character, Callback onMatch) {
    ShiftWordOccurrencesCounters();
    state_ = state_.Next(character);
    state_.ForEachMatch([&](size_t patternId) { UpdateWordOccurrencesCounters(patternId); });
    //Если нашли нужное количество слов - то нашли шаблон поэтому onMatch

    if (occurrencesByOffset_.front() == numberOfWords_) {
        onMatch();
    }
}

WildcardMatcher::WildcardMatcher(const std::string &pattern, char wildcard) {
    AhoCorasickBuilder ahoCorasickBuilder;
    std::vector<std::string> patternsParts = Split(pattern, wildcard, ahoCorasickBuilder);

    automaton_ = ahoCorasickBuilder.Build();
    state_ = automaton_->getRoot();
    numberOfWords_ = patternsParts.size();
    patternLength_ = pattern.length();

}

int main() {
    std::string pattern;
    std::string text;
    std::cin >> pattern;
    std::cin >> text;
    WildcardMatcher matcher(pattern, '?');

    for (size_t i = 0; i < text.size(); ++i) {
        matcher.Scan(text[i], [&]() {
          if (i + 1 >= pattern.length()) {
              std::cout << i - pattern.length() + 1 << " ";
          }

        });
    }
    return 0;
}