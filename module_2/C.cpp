//
// Created by lida on 14.10.18.
//
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <numeric>
const size_t ALPHABET_SIZE = 256;
const char FIRST_DELIMITER = '#';
const char SECOND_DELIMITER = '$';

namespace builder {
    std::vector<size_t> countFirstStepOrder_(const std::string &str) {
        std::vector<size_t> order(str.length()); //Вектор перестановок строки
        std::vector<size_t> counter(ALPHABET_SIZE); //Количество вхождения данного символа в строку

        //Заполняем вектор количества вхождений
        for (char i : str) {
            counter[i]++;
        }

        //Разделим все на группы (классы эквивалентности)
        std::partial_sum(counter.begin(), counter.end(), counter.begin());

        //Заполняем вектор перестановок строки
        for (size_t i = 0; i < str.length(); ++i) {
            order[--counter[str[i]]] = i;
        }
        return order;
    }

    std::vector<size_t> buildSuffixArray(const std::string &str) {
        std::vector<size_t> order = countFirstStepOrder_(str);
        size_t n = str.length();
        std::vector<size_t> colours(n);
        colours[order[0]] = 0;
        size_t colourAmount = 1;
        for (size_t i = 1; i < n; ++i) {
            if (str[order[i]] != str[order[i - 1]]) {
                ++colourAmount;
            }
            colours[order[i]] = colourAmount - 1;
        }

        // sort BY second element
        for (size_t k = 0; (1 << k) < n; ++k) {

            std::vector<size_t> order_n(n);
            std::vector<size_t> colours_n(n);

            for (size_t i = 0; i < n; ++i) {
                if (order[i] >= (1 << k)) {
                    order_n[i] = order[i] - (1 << k);//Пока что один пытаемся получить второй шаг
                } else {
                    order_n[i] = order[i] + n - (1 << k);
                }
            }

            for (size_t i = 0; i < n; ++i) {
                ++colours_n[colours[order_n[i]]];
            }

            for (size_t i = 1; i < colourAmount; ++i) {
                colours_n[i] += colours_n[i - 1];
            }

            for (size_t i = 0; i < n; ++i) {
                order[--colours_n[colours[order_n[i]]]] = order_n[i];
            }

            colours_n[order[0]] = 0;
            colourAmount = 1;

            for (size_t i = 1; i < n; ++i) {
                size_t currentSybmolIndex = (order[i] + (1 << k)) % n;
                size_t previousSybmolIndex = (order[i - 1] + (1 << k)) % n;
                if ((colours[order[i]] != colours[order[i - 1]])
                    || (colours[currentSybmolIndex] != colours[previousSybmolIndex])) {
                    ++colourAmount;
                }
                colours_n[order[i]] = colourAmount - 1;
            }

            colours = std::move(colours_n);

        }

        return order;
    }
    std::vector<size_t> buildLcpArray(const std::vector<size_t> &suffArray, const std::string &str) {
        size_t n = suffArray.size();
        std::vector<size_t> lcp(n);
        std::vector<size_t> rank(n); //Обратный к суффиксному массиву

        for (size_t i = 0; i < n; i ++) {
            rank[suffArray[i]] = i;
        }
        size_t k = 0; //Length of previous LCP

        for (size_t i = 0; i < n; i++) {

            if(rank[i] == n - 1) {
                k = 0;
                continue;
            }

            size_t j = suffArray[rank[i] + 1];

            while(i + k < n && j + k < n && str[i + k] == str[j + k]) {
                k++;
            }

            lcp[rank[i]] = k;
            if (k > 0) {
                k--;
            }
        }
        lcp.pop_back();
        return lcp;
    }
}


class CommonStringFinder {
public:
    CommonStringFinder(const std::string &firstString, const std::string &secondString, const size_t &index): index_(index) {
        sharpIndex_ = firstString.length();
        dollarIndex_ = sharpIndex_ + secondString.length() + 1;
        resultString_ = buildString(firstString, secondString);
    };

    template <class Callback>
    void findCommonString(Callback callback);

private:
    std::size_t sharpIndex_;
    std::size_t dollarIndex_;
    std::size_t index_;
    std::string resultString_;
    bool areDifferent(const size_t &leftStringIndex, const size_t &rightStringIndex);
    const std::string buildString(const std::string &firstString, const std::string &secondString);

};

template<class Callback>
void CommonStringFinder::findCommonString(Callback callback) {
    std::vector<size_t> order = builder::buildSuffixArray(resultString_);
    std::vector<size_t> lcp = builder::buildLcpArray(order, resultString_);
    size_t minLCP = 0;
    for (size_t i = 1; i < lcp.size() && index_ > 0; i++) {
        size_t newStringAmount = 0;
        if (areDifferent(order[i], order[i + 1])) {
            if (lcp[i] >= minLCP) {
                newStringAmount = lcp[i] - minLCP;
                minLCP = lcp[i];
            } else {
                minLCP = lcp[i];
            }

        } else {
            minLCP = std::min(minLCP, lcp[i]);
        }

        // Нельзя отнимать больше чем есть
        if (newStringAmount >= index_) {
            std::string commonString = resultString_.substr(order[i], minLCP - newStringAmount + index_);
            callback(commonString);
            index_ = 0;
        } else {
            index_ -= newStringAmount;
        }

    }

    if (index_ != 0) {
        int stringNotFound = -1;
        callback(stringNotFound);
    }
}

bool CommonStringFinder::areDifferent(const size_t &leftStringIndex, const size_t &rightStringIndex) {
    bool areDifferent = false;
    if (((leftStringIndex < sharpIndex_) && (rightStringIndex > sharpIndex_) && (leftStringIndex < dollarIndex_))
        || ((leftStringIndex > sharpIndex_) && (leftStringIndex < dollarIndex_) && (rightStringIndex < sharpIndex_))) {
        areDifferent = true;
    }
    return areDifferent;
}

const std::string CommonStringFinder::buildString(const std::string &firstString, const std::string &secondString) {
    return firstString + FIRST_DELIMITER + secondString + SECOND_DELIMITER;;
}

int main() {
    std::string firstString;
    std::string secondString;
    size_t index;

    std::cin >> firstString;
    std::cin >> secondString;
    std::cin >> index;

    CommonStringFinder stringFinder(firstString, secondString, index);
    stringFinder.findCommonString([](auto &commonString) {std::cout << commonString << std::endl;});
    return 0;
}