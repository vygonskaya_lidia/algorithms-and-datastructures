//
// Created by lida on 25.11.18.
//
#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>

constexpr double INFTY = std::numeric_limits<double>::max();
constexpr size_t DIFFERENT_EVENTS = 6;

struct Point {
  double x;
  double y;
  double z;
  int id;
  Point *next, *prev;

  explicit Point(double x = 0, double y = 0, double z = 0, int id = -1)
      : x(x), y(y), z(z), id(id), next(nullptr), prev(nullptr) {};

  bool performEvent() {
      if (prev->next != this) {
          prev->next = this;
          next->prev = this;
          return true;
      } else {
          prev->next = next;
          next->prev = prev;
          return false;
      }
  };

  friend bool operator < (const Point &firstPoint, const Point &secondPoint) {
      return firstPoint.x < secondPoint.x;
  };
};



struct Facet {
  int firstVertex;
  int secondVertex;
  int thirdVertex;

  Facet(int firstVertex, int secondVertex, int thirdVertex)
      : firstVertex(firstVertex), secondVertex(secondVertex), thirdVertex(thirdVertex) {}

  void reorderVertices() {
      if (secondVertex < firstVertex && secondVertex < thirdVertex) {
          std::swap(firstVertex, secondVertex);
          std::swap(secondVertex, thirdVertex);
      } else if (thirdVertex < firstVertex && thirdVertex < secondVertex) {
          std::swap(secondVertex, thirdVertex);
          std::swap(firstVertex, secondVertex);
      }
  }

  friend bool operator<(const Facet& firstFacet, const Facet& secondFacet) {
      if (firstFacet.firstVertex < secondFacet.firstVertex) {
          return true;
      } else if (firstFacet.firstVertex > secondFacet.firstVertex) {
          return false;
      } else {
          if (firstFacet.secondVertex < secondFacet.secondVertex) {
              return true;
          } else if (firstFacet.secondVertex > secondFacet.secondVertex) {
              return false;
          } else {
              return firstFacet.thirdVertex < secondFacet.thirdVertex;
          }
      }
  }
};


/*

 Функция определяет по часово или против часовой стрелке идут три заданные точки
 Число меньше нуля, если 3 точки расположены по часовой стрелке

 */
double clockwise(const Point* a, const Point* b, const Point* c) {
    if (a == nullptr || b == nullptr || c == nullptr) {
        return INFTY;
    }
    return (b->x - a->x) * (c->y - b->y) - (b->y - a->y) * (c->x - b->x);
}


// Когда поворот меняется. Как бы считаем время, когда он поменялся
double time(const Point* a, const Point* b, const Point* c) {
    if (a == nullptr || b == nullptr || c == nullptr) {
        return INFTY;
    }
    return ((b->x - a->x) * (c->z - b->z) - (b->z - a->z) * (c->x - b->x)) / clockwise(a, b, c);
}



typedef Point Event;


// Строим "нижнюю оболочку" все точки лежат lowerLine - "нижняя линия"
std::vector<Event*> buildDownHull(std::vector<Point>& points, size_t leftPart, size_t rightPart) {
    if (rightPart - leftPart == 1) {
        return std::vector<Event*>();
    }
    size_t middle = (leftPart + rightPart) / 2;
    std::vector<Event*> events[2] = { // Массив из векторов событий
        buildDownHull(points, leftPart, middle),
        buildDownHull(points, middle, rightPart)
    };
    std::vector<Event*> result;

    Point* u = &points[middle - 1];
    Point* v = &points[middle];

    // Находим первый "мостик". Делаем это пока все три точки не будут образовывать поворот против часовой стрелки
    for (;;) {
        if (clockwise(u, v, v->next) < 0) { // Если поворот по часовой стрелке
            v = v->next;
        } else if (clockwise(u->prev, u, v) < 0) { // Если поворот по часовой стрелке
            u = u->prev;
        } else {
            break;
        }
    }

    size_t performedEventsLeft = 0, performedEventsRight = 0;
    for (double current_t = -INFTY;;) {

        Point* left = nullptr; // Левая точка мостика
        Point* right = nullptr; // Правая точка мостика
        std::vector<double> next_t(DIFFERENT_EVENTS, INFTY); // Всего есть 6 возможных случаев расположения точек

        /*
         * Чтобы определить какой из следующих 6 возможностей нужно запустить. Считаем 6 значений "времени" на которых
         * появляются эти случаи и берем из них минимальный
         */
        if (performedEventsLeft < events[0].size()) {
            left = events[0][performedEventsLeft]; // Если из/в L удалили или вставили новую вершину, то это то же, что новая вершнина - слева от текущей
            next_t[0] = time(left->prev, left, left->next);
        }
        if (performedEventsRight < events[1].size()) {
            right = events[1][performedEventsRight]; // Если из/в L удалили или вставили новую вершину, то это то же, что новая вершнина - справа от текущей
            next_t[1] = time(right->prev, right, right->next);
        }
        next_t[2] = time(u, v, v->next); // Если по часовой стрелке, то новый мост - u->prev v
        next_t[3] = time(u, v->prev, v); // Если против часовой стрелки, то новый мост - u v->prev
        next_t[4] = time(u->prev, u, v); // Если по часовой стрелке то новый мост - u->prev v
        next_t[5] = time(u, u->next, v); // Если против часовой стрелки, то новый мост - u->next v

        int min_i = -1;
        double min_t = INFTY;
        for (int i = 0; i < DIFFERENT_EVENTS; ++i) {
            if (next_t[i] > current_t && next_t[i] < min_t) {
                min_t = next_t[i];
                min_i = i;
            }
        }
        if (min_i == -1 || min_t == INFTY) {
            break;
        }

        switch (min_i) {
            case 0:
                if (left->x < u->x) {
                    result.push_back(left);
                }
                left->performEvent();
                ++performedEventsLeft;
                break;
            case 1:
                if (right->x > v->x) {
                    result.push_back(right);
                }
                right->performEvent();
                ++performedEventsRight;
                break;
            case 2:
                result.push_back(v);
                v = v->next;
                break;
            case 3:
                v = v->prev;
                result.push_back(v);
                break;
            case 4:
                result.push_back(u);
                u = u->prev;
                break;
            case 5:
                u = u->next;
                result.push_back(u);
                break;
            default:
                break;
        }
        current_t = min_t;
    }

    // Возвращаемся "назад во времени и обновляем точки"
    u->next = v;
    v->prev = u;

    for (auto it = result.rbegin(); it != result.rend(); ++it) {
        Point* current = *it;
        if (current->x > u->x && current->x < v->x) {
            u->next = v->prev = current;
            current->prev = u;
            current->next = v;
            if (current->x <= points[middle - 1].x) {
                u = current;
            } else {
                v = current;
            }
        } else {
            current->performEvent();
            if (current == u) {
                u = u->prev;
            }
            if (current == v) {
                v = v->next;
            }
        }
    }
    return result;
}



std::vector<Facet> buildConvexHull(std::vector<Point> points) {
    size_t n = points.size();
    std::vector<Facet> result;
    std::sort(points.begin(), points.end());
    std::vector<Event*> events = buildDownHull(points, 0, n);
    for (Event* e : events) {
        Facet current(e->prev->id, e->id, e->next->id);
        if (!e->performEvent()) {
            std::swap(current.firstVertex, current.secondVertex);
        }
        result.push_back(current);
    }
    for (Point& point : points) {
        point.next = nullptr;
        point.prev = nullptr;
        point.z = -point.z;
    }
    events = buildDownHull(points, 0, n);
    for (Event* e : events) {
        Facet current(e->prev->id, e->id, e->next->id);
        if (e->performEvent()) {
            std::swap(current.firstVertex, current.secondVertex);
        }
        result.push_back(current);
    }
    return result;
}



void rotate(Point& point, double angle) {

    double new_x = point.x * cos(angle) + point.z * sin(angle);
    double new_z = -point.x * sin(angle) + point.z * cos(angle);
    point.x = new_x;
    point.z = new_z;

    new_z = point.z * cos(angle) + point.y * sin(angle);
    double new_y = -point.z * sin(angle) + point.y * cos(angle);
    point.z = new_z;
    point.y = new_y;

    new_x = point.x * cos(angle) + point.y * sin(angle);
    new_y = -point.x * sin(angle) + point.y * cos(angle);
    point.x = new_x;
    point.y = new_y;
}


int main() {
    int testsNumber; // Количество тестов
    std::cin >> testsNumber;
    for(size_t i = 0; i < testsNumber; ++i) {
        int pointsNumber;
        std::vector<Point> points;

        std::cin >> pointsNumber; // Количество точек
        for (int j = 0; j < pointsNumber; ++j) {
            int x, y, z;
            std::cin >> x >> y >> z;
            Point point(x, y, z, j);
            rotate(point, 0.01); // Устраняем шумы при помощи поворота точек
            points.push_back(point);
        }

        std::vector<Facet> hull = buildConvexHull(points);
        for (Facet& facet : hull) {
            facet.reorderVertices();
        }

        std::sort(hull.begin(), hull.end());
        std::cout << hull.size() << "\n";
        for (Facet& facet : hull) {
            std::cout << 3 << " " << facet.firstVertex << " " << facet.secondVertex << " " << facet.thirdVertex << "\n";
        }
    }
    return 0;
}