//
// Created by lida on 18.11.18.
//
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

constexpr double EPS = 1e-4;

struct Vector {
  double x;
  double y;
  Vector(double x, double y): x(x), y(y) {};

  friend Vector operator-(const Vector &firstVector, const Vector &secondVector);

  friend Vector operator+(const Vector &firstVector, const Vector &secondVector);

};

Vector operator+(const Vector &firstVector, const Vector &secondVector) {
    return Vector(firstVector.x + secondVector.x, firstVector.y + secondVector.y);
}

bool isPolarAngleSmaller(const Vector &firstVector, const Vector &secondVector) {
    // Возвращает True - если полярный угол на первый вектор меньше чем второй
    if (firstVector.x > 0 && secondVector.x < 0) {
        return true;
    }
    if (firstVector.x < 0 && secondVector.x > 0) {
        return false;
    }
    
    double firstAngleTg = firstVector.y / firstVector.x;
    double secondAngleTg = secondVector.y / secondVector.x;

    if (fabs(firstVector.x) < EPS) {
        if (firstVector.y > 0) {
            return secondVector.x < 0;
        } else {
            return false;
        }
    }
    if (fabs(secondVector.x) < EPS) {
        if (secondVector.y > 0) {
            return firstVector.x > 0;
        } else {
            return fabs(firstAngleTg - secondAngleTg) > EPS;
        }
    }

    if (fabs(firstAngleTg - secondAngleTg) < EPS) {
        return false;
    }
    return firstAngleTg < secondAngleTg;
}
std::vector<Vector> countMinkovskiySumm(std::vector<Vector> &firstPolygon, std::vector<Vector> &secondPolygon) {
    size_t n = firstPolygon.size();
    size_t m = secondPolygon.size();
    std::vector<Vector> minkovskiySumm;
    firstPolygon.push_back(firstPolygon[0]); // Добавляем последней вершиной первую для замыкания
    secondPolygon.push_back(secondPolygon[0]);
    firstPolygon.push_back(firstPolygon[1]); // Добавляем последней вершиной первую для замыкания
    secondPolygon.push_back(secondPolygon[1]);
    size_t i = 0;
    size_t j = 0;
    // Хотим найти только самые крайние вершины оболочки
    while (i < n && j < m) {
        minkovskiySumm.push_back(firstPolygon[i] + secondPolygon[j]);

        if ((isPolarAngleSmaller(firstPolygon[i + 1] - firstPolygon[i], secondPolygon[j + 1] - secondPolygon[j]))) {
            ++i;
        } else if (isPolarAngleSmaller(secondPolygon[j + 1] - secondPolygon[j], firstPolygon[i + 1] - firstPolygon[i])) {
            ++j;
        } else {
            ++i;
            ++j;
        }
    }
    return minkovskiySumm;
}


std::vector<Vector> reOrderVertices(const std::vector<Vector> &polygon) {
    //Упорядочивает вершины многоугольника. Против часовой стрелки начиная с самой нижней и самой левой вершины
    std::vector<Vector> orderedPolygon;
    size_t n = polygon.size();
    size_t firstVertexIndex = 0;

    for (size_t i = 1; i < n; ++i) {
        if (((polygon[i].x < polygon[firstVertexIndex].x) && (fabs(polygon[i].y - polygon[firstVertexIndex].y) < EPS))
            || polygon[i].y < polygon[firstVertexIndex].y) {
            firstVertexIndex = i;
        }
    }

    for (size_t i = 0; i < polygon.size(); ++i) {
        orderedPolygon.push_back(polygon[(firstVertexIndex + i + 1) % n]);
    }
    std::reverse(orderedPolygon.begin(), orderedPolygon.end());
    return orderedPolygon;
}


Vector operator-(const Vector &firstVector, const Vector &secondVector) {
    return Vector(firstVector.x - secondVector.x, firstVector.y - secondVector.y);
}


bool isZeroInResultPolygon(const std::vector<Vector> &minkovskiySumm) {
    /* Если ноль лежит внутри фигуры, которая получилась в результате подсчета суммы минковского, то
    в таком случае многоугольники пересекаются
    */
    bool isZeroInResultPolygon = false;
    for (size_t i = 0, j = minkovskiySumm.size() - 1; i < minkovskiySumm.size(); j = i++) {
        if ((((minkovskiySumm[i].y <= 0.0) && (0.0 < minkovskiySumm[j].y))
            || ((minkovskiySumm[j].y <= 0.0) && (0.0 < minkovskiySumm[i].y)))
            && (0.0 > (minkovskiySumm[j].x - minkovskiySumm[i].x) * (0.0 - minkovskiySumm[i].y)
                / (minkovskiySumm[j].y - minkovskiySumm[i].y) + minkovskiySumm[i].x)) {
            isZeroInResultPolygon = !isZeroInResultPolygon;
        }
    }
    return isZeroInResultPolygon;
}

std::vector<Vector> readPolygon(bool inverted=false) {
    size_t polygonVerticesCount;
    std::vector<Vector> polygon;
    std::cin >> polygonVerticesCount;
    for (size_t i = 0; i < polygonVerticesCount; ++i) {
        double x;
        double y;
        std::cin >> x >> y;
        if (inverted) {
            polygon.emplace_back(-x, -y);
        } else {
            polygon.emplace_back(x, y);
        }
    }
    polygon = reOrderVertices(polygon);
    return polygon;
}

int main() {
    std::vector<Vector> firstPolygon = readPolygon();
    std::vector<Vector> secondPolygon = readPolygon(true);

    std::vector<Vector> minkovskiySumm = countMinkovskiySumm(firstPolygon, secondPolygon);

    if (isZeroInResultPolygon(minkovskiySumm)) {
        std::cout << "YES" << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }
    return 0;
}
