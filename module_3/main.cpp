#include <iostream>
#include <cmath>
#include <iomanip>

constexpr double EPS = 0.00000001;
const int precision = 8;

struct Point {
  Point (double x, double y, double z): x(x), y(y), z(z) {};
  double x;
  double y;
  double z;
};

struct Vector {
  Vector(Point beginPoint, Point endPoint) {
      x = endPoint.x - beginPoint.x;
      y = endPoint.y - beginPoint.y;
      z = endPoint.z - beginPoint.z;
  };


  // Координаты направляющего вектора в пространстве
  double x;
  double y;
  double z;

  double operator * (const Vector &v){
      return this->x * v.x + this->y * v.y + this->z * v.z;
  }
};

struct Segment {
  Segment(Point beginPoint, Point endPoint)
      : beginPoint(beginPoint), endPoint(endPoint), leadingVector(Vector(beginPoint, endPoint)) {};

  Point beginPoint;
  Point endPoint;
  Vector leadingVector; // Направляющий вектор.

  Vector getVector() {
      return this->leadingVector;
  }

};

double countSegmentDist(Segment firstSegment, Segment secondSegment) {

    double a = firstSegment.getVector() * firstSegment.getVector();
    double b = firstSegment.getVector() * secondSegment.getVector();

    double c = secondSegment.getVector() * secondSegment.getVector();

    Vector fromOneSegmentToAnother(secondSegment.beginPoint, firstSegment.beginPoint);

    double d = firstSegment.getVector() * fromOneSegmentToAnother;
    double e = fromOneSegmentToAnother * secondSegment.getVector();

    double D = a * c - b * b;

    double sc, sN, sD = D;
    double tc, tN, tD = D;


    // В таком случае отрезки параллельны
    if (D <= EPS) {
        sN = 0.0;
        sD = 1.0;
        tN = e;
        tD = c;

    } else {
        sN = b * e - c * d;
        tN = a * e - b * d;

        if (sN < 0.0) {
            sN = 0.0;
            tN = e;
            tD = c;
        } else if (sN >= sD) {
            sN = sD;
            tN = e + b;
            tD = c;
        }
    }

    if (tN <= 0.0) {
        tN = 0.0;

        if (-d <= 0.0) {
            sN = 0.0;
        }

        else if (-d >= a) {
            sN = sD;
        }

        else {
            sN = -d;
            sD = a;
        }
    } else if (tN >= tD) {
        tN = tD;
        if ((-d + b) <= 0.0)
            sN = 0;
        else if ((-d + b) >= a)
            sN = sD;
        else
        {
            sN = -d + b;
            sD = a;
        }
    }

    if (std::abs(sN) <= EPS) {
        sc = 0.0;
    } else {
        sc = sN / sD;
    }

    if (std::abs(tN) <= EPS) {
        tc = 0.0;
    } else {
        tc = tN / tD;
    }

    //Вектор соединяющий две близжайшие точки
    Point firstSegmentClosestPoint(
        fromOneSegmentToAnother.x + (sc * firstSegment.getVector().x) - (tc * secondSegment.getVector().x),
        fromOneSegmentToAnother.y + (sc * firstSegment.getVector().y) - (tc * secondSegment.getVector().y),
        fromOneSegmentToAnother.z + (sc * firstSegment.getVector().z) - (tc * secondSegment.getVector().z)

        );
    Point secondSegmentClosestPoint(0.0, 0.0, 0.0);
    Vector closestPointsVector(secondSegmentClosestPoint, firstSegmentClosestPoint);
    double squaredLength = closestPointsVector * closestPointsVector;
    double answer = std::sqrt(squaredLength);
    return answer;
};

int main() {
    double x1, y1, z1, x2, y2, z2;
    double x3, y3, z3, x4, y4, z4;
    std::cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
    std::cin >> x3 >> y3 >> z3 >> x4 >> y4 >> z4;
    Point firstPoint(x1, y1, z1);
    Point secondPoint(x2, y2, z2);
    Point thirdPoint(x3, y3, z3);
    Point fourthPoint(x4, y4, z4);
    Segment firstSegment(firstPoint, secondPoint);
    Segment secondSegment(thirdPoint, fourthPoint);
    double answer = countSegmentDist(firstSegment, secondSegment);
    std::cout << std::setprecision(precision) << answer << std::endl;
    return 0;
}


